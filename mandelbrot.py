#   numpy-mandelbrot
#   Copyright (C) 2021  Christian Calderon
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

from multiprocessing import Process, cpu_count
from multiprocessing.shared_memory import SharedMemory
from numpy import (
    ndarray, arange, array, zeros, zeros_like, copyto, dtype,
    add, square, abs, greater, equal, logical_and, multiply,
    log1p, log2, log, negative, fmod, copy,
    ubyte, uint64, float64, complex128, bool_
)
from scipy.interpolate import CubicSpline
from typing import Callable, ClassVar
from os import urandom
from json import dump
from time import gmtime, strftime
from png import Writer


Color = tuple[int, int, int]
ColorDType = dtype([('r', ubyte), ('g', ubyte), ('b', ubyte)])

Spline = Callable[[ndarray, ndarray, ndarray], None]


def colors_to_spline(colors: list[Color]) -> Spline:
    max_val = 255

    reds, greens, blues = map(list, zip(*colors))

    reds.extend(reds[-2::-1])
    greens.extend(greens[-2::-1])
    blues.extend(blues[-2::-1])

    color_count = len(reds)

    reds = array(reds, dtype=float64)

    greens = array(greens, dtype=float64)

    blues = array(blues, dtype=float64)

    x = arange(color_count, dtype=float64)

    red_spline = CubicSpline(x, reds)
    green_spline = CubicSpline(x, greens)
    blue_spline = CubicSpline(x, blues)


    def spline(smooth_iters, out, where):

        fmod(smooth_iters, color_count, out=smooth_iters)
        temp = red_spline(smooth_iters)
        copyto(out['r'], temp.astype(ubyte), where=where)
        del temp
        temp = green_spline(smooth_iters)
        copyto(out['g'], temp.astype(ubyte), where=where)
        del temp
        temp = blue_spline(smooth_iters)
        copyto(out['b'], temp.astype(ubyte), where=where)
        del temp

    return spline



def worker(shmem_name: str,
           pixel_width: int,
           pixel_height: int,
           row_offset: int,
           row_step: int,
           max_iterations: int,
           center_real: float,
           center_imag: float,
           zoom: float,
           mag_threshold: float,
           colors: list[Color]):
    """Computes part of a Mandelbrot fractal image."""
    shared_mem = SharedMemory(name=shmem_name)
    pixels = ndarray(shape=(pixel_height, pixel_width),
                     dtype=ColorDType,
                     buffer=shared_mem.buf)

    spline = colors_to_spline(colors)

    iterations = arange(max_iterations, dtype=uint64)

    rows = arange(row_offset, pixel_height, row_step, dtype=uint64)

    imag_step = -row_step/zoom*1j
    imag_start = (center_imag + pixel_height/zoom/2 - 1/zoom/2 - row_offset/zoom)*1j

    c = arange(center_real - pixel_width/zoom/2 + 1/zoom/2,
               center_real + pixel_width/zoom/2,
               1/zoom,
               dtype=complex128)
    c += imag_start

    c_copy = zeros_like(c)
    z = zeros_like(c)
    check = zeros_like(z, dtype=bool_)
    mag = zeros_like(z, dtype=float64)
    smooth_iters = zeros_like(z, dtype=float64)


    for row in rows:
        copyto(c_copy, c)
        copyto(z, c)
        for n in iterations:
            square(z, out=z)
            add(z, c_copy, out=z)
            abs(z, out=mag)
            greater(mag, mag_threshold, out=check)
            z[check] = 0.0
            c_copy[check] = 0.0
            equal(smooth_iters, 0.0, out=check, where=check)
            log(mag, out=smooth_iters, where=check)
            log2(smooth_iters, out=smooth_iters, where=check)
            negative(smooth_iters, out=smooth_iters, where=check)
            add(smooth_iters, n + 1, out=smooth_iters, where=check)
            log1p(smooth_iters, out=smooth_iters, where=check)

        greater(smooth_iters, 0.0, out=check)
        spline(smooth_iters, pixels[row], check)

        add(c, imag_step, out=c)
        check[:] = 0
        mag[:] = 0
        smooth_iters[:] = 0

    del pixels
    shared_mem.close()


def timestamp():
    return strftime('%d-%m-%Y-%H-%M-%S-GMT', gmtime())


def main():
    color_count = 3
    colors = [tuple(urandom(3)) for _ in range(color_count)]
    pixel_width = 38400
    pixel_height = 21600
    max_iters = 100
    center_real = -0.5
    center_imag = 0.0
    zoom = 10000.0
    mag_threshold = 200.0
    sharedmem = SharedMemory(create=True, size=pixel_width*pixel_height*ColorDType.itemsize)
    shmem_name = sharedmem.name

    procs = []
    proc_count = cpu_count()
    for p in range(proc_count):
        args = (
            shmem_name,
            pixel_width,
            pixel_height,
            p,
            proc_count,
            max_iters,
            center_real,
            center_imag,
            zoom,
            mag_threshold,
            colors
        )
        procs.append(Process(target=worker, args=args))
        procs[-1].start()

    for proc in procs:
        proc.join()

    pixels = ndarray(
        shape=(pixel_height, 3*pixel_width),
        dtype=ubyte,
        buffer=sharedmem.buf
    )

    result_timestamp = timestamp()
    metadata = {
        'colors': colors,
        'resolution': (pixel_width, pixel_height),
        'max_iterations': max_iters,
        'center': (center_real, center_imag),
        'zoom': zoom,
        'magnitude_threshold': mag_threshold
    }
    img_name = f'mandelbrot-{result_timestamp}.png'
    metadata_name = f'mandelbrot-{result_timestamp}.json'

    with open(metadata_name, 'w') as m:
        dump(metadata, m, sort_keys=True, indent=4)

    with open(img_name, 'wb') as img:
        writer = Writer(width=pixel_width, height=pixel_height, greyscale=False)
        writer.write(img, pixels)

    del pixels
    sharedmem.close()
    sharedmem.unlink()

if __name__ == '__main__':
    main()
